var kursService = require('../services/KursService')
const { body, query, param} = require('express-validator')
const CONSTANT = require('../utils/constant')
const { validationResult } = require('express-validator/check')

class KursController {

    async getKurs(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          res.status(400).json({ errors: errors.array() });
          return errors;
        }
        let startDate = new Date(req.query.startdate)
        let endDate = new Date(req.query.enddate)
        try {
            let result = await kursService.getKurs(startDate, endDate)
            res.json(result)
        } catch (err) {
            next(err)
        }
    }

    async getKursBySymbol(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          res.status(400).json({ errors: errors.array() });
          return errors;
        }
        let startDate = new Date(req.query.startdate)
        let endDate = new Date(req.query.enddate)
        try {
            let result = await kursService.getKursBySymbol(req.params.symbol, startDate, endDate)
            res.json(result)
        } catch (err) {
            next(err)
        }
    }

    async removeKursByDate(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          res.status(400).json({ errors: errors.array() });
          return errors;
        }
        let date = new Date(req.params.date)
        try {
            await kursService.removeKursByDate(date)
            res.sendStatus(200)
        } catch (err) {
            next(err)
        }
    }
    
    async addKurs(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          res.status(400).json({ errors: errors.array() });
          return errors;
        }
        try {
            let isExist = await kursService.isKursExist(req.body.symbol, req.body.refresh_date)
            if (isExist) {
                res.status(409).json({ msg: CONSTANT.DUPLICATE });
                return
            }
            kursService.addKurs(req.body)
            res.sendStatus(201)
        } catch (err) {
            next(err)
        }
    }
    
    async updateKurs(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          res.status(400).json({ errors: errors.array() });
          return errors;
        }
        try {
            kursService.updateKurs(req.body)
            res.sendStatus(200)
        } catch (err) {
            next(err)
        }
    }

    validate(method) {
        switch(method) {
            case 'searchByDate': {
                return [ 
                    query('startdate', CONSTANT.INVALID_DATE).custom(value => {
                        return !isNaN(Date.parse(value));
                    }),
                    query('enddate', CONSTANT.INVALID_DATE).custom(value => {
                        return !isNaN(Date.parse(value));
                    })
                   ]   
            }
            case 'removeByDate': {
                return [
                    param('date', CONSTANT.INVALID_DATE).custom(value => {
                        return !isNaN(Date.parse(value));
                    })
                ]
            }
            case 'kurs': {
                return [
                    body(['ERate.buy', 'ERate.sell', 'TT.buy', 'TT.sell', 'BN.buy', 'BN.sell'], CONSTANT.NAN).isNumeric()
                ]
            }
        }
    }
}

module.exports = new KursController()
