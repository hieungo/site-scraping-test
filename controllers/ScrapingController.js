var scrapingService = require('../services/ScrapingService')

class ScrapingController {

    async scrape(req, res, next) {
        try {
            await scrapingService.scrape()
            res.sendStatus(200)
        } catch (err) {
            next(err)
        }
    }

}

module.exports = new ScrapingController()