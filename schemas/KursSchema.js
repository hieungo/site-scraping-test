const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const kursSchema = new Schema({
    _id: String,
    symbol: String,
    ERate: {
        sell: Number,
        buy: Number
    },
    TT: {
        sell: Number,
        buy: Number
    },
    BN: {
        sell: Number,
        buy: Number
    },
    refresh_date: Date,
})

module.exports = mongoose.model('Kurs', kursSchema)