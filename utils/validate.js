const { validationResult } = require('express-validator/check')

exports.reqCheck = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
      return errors;
    }
}