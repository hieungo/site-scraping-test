module.exports = Object.freeze({
    //scraping config
    BCA_URL: 'https://www.bca.co.id/en/informasi/kurs',
    TABLE_ROW: '[code]',
    CODE: 'code',
    REFRESH_DATE: '.refresh-date',
    RATE_TYPE: '[rate-type]',
    RATE_TYPE_ATTR: 'rate-type',
    RATE_TYPE_DELIMETER: '-',
    //kurls status
    BUY: 'buy',
    SELL: 'sell',
    //error message
    INVALID_DATE: 'invalid date',
    NAN: 'invalid number',
    DUPLICATE: 'duplicate record',
    //noti message
    DB_SUCCESS: 'database connection successful',
    DB_FAIL: 'database connection error'
});