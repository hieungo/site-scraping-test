//The require(‘mongoose’) call above returns a Singleton object. 
//It means that the first time you call require(‘mongoose’), it 
//is creating an instance of the Mongoose class and returning it. 
//On subsequent calls, it will return the same instance that was 
//created and returned to you the first time because of how module 
//import/export works in ES6.
const CONSTANT = require('../utils/constant')
const mongoose = require("mongoose");
require('dotenv-expand')(require('dotenv').config()); 

class Database {

    constructor() {
        let uri = process.env.MONGODB_URI;
        this.connect(uri);
        
    }
    
    connect(uri) {
        mongoose.connect(uri, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true
        })
        .then(() => {
            console.log(CONSTANT.DB_SUCCESS);
        })
        .catch((err) => {
            console.log(CONSTANT.DB_FAIL + ' ' + err);
        })
    }
}

module.exports = new Database();