const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');

chai.should()
chai.use(chaiHttp)

describe('API tests', () => {
  describe('Indexing', function() {
    it('it should return status 200', function(done) {
      chai.request(app)
        .get('/api/indexing')
        .end((err, res) => {
          res.should.have.status(200)
          done()
        })
    })
  }),
  describe('GET kurs success', function() {
    it('it should return status 200', function(done) {
      chai.request(app)
        .get('/api/kurs?startdate=2021-05-24&enddate=2021-05-24')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('array')
          done()
        })
    })
  }),
  describe('GET kurs with invalid start date', function() {
    it('it should return status 400', function(done) {
      chai.request(app)
        .get('/api/kurs?startdate=2021-05-24aaaa&enddate=2021-05-24')
        .end((err, res) => {
          res.should.have.status(400)
          done()
        })
    })
  }),
  describe('GET kurs with invalid end date', function() {
    it('it should return status 400', function(done) {
      chai.request(app)
        .get('/api/kurs?startdate=2021-05-24aaaa&enddate=2021-05-241111111')
        .end((err, res) => {
          res.should.have.status(400)
          done()
        })
    })
  }),
  describe('GET kurs by symbol', function() {
    it('it should return status 200', function(done) {
      chai.request(app)
        .get('/api/kurs/USD?startdate=2021-05-24&enddate=2021-05-24')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('array')
          done()
        })
    })
  }),
  describe('GET kurs by symbol with invalid start date', function() {
    it('it should return status 400', function(done) {
      chai.request(app)
        .get('/api/kurs/USD?startdate=2021-05-24aaaaa&enddate=2021-05-24')
        .end((err, res) => {
          res.should.have.status(400)
          done()
        })
    })
  }),
  describe('GET kurs by symbol with invalid end date', function() {
    it('it should return status 400', function(done) {
      chai.request(app)
        .get('/api/kurs/USD?startdate=2021-05-24&enddate=2021-05-2411111')
        .end((err, res) => {
          res.should.have.status(400)
          done()
        })
  }),
  describe('POST kurs success', function() {
    it('it should return status 201', function(done) {
      chai.request(app)
        .post('/api/kurs')
        .send({
          ERate: {
              'buy': 14357,
              'sell': 14357
          },
          TT: {
              'buy': 14197,
              'sell': 14497
          },
          BN: {
              'buy': 14222,
              'sell': 14522
          },
          refresh_date: '2021-05-24T00:00:00.000Z',
          symbol: 'USD'
        })
        .end((err, res) => {
          res.should.have.status(201)
          done()
        })
      })
    }),
  describe('POST kurs invalid number', function() {
    it('it should return status 400', function(done) {
      chai.request(app)
        .post('/api/kurs')
        .send({
          ERate: {
              'buy': 'asd',
              'sell': 14357
          },
          TT: {
              'buy': 14197,
              'sell': 14497
          },
          BN: {
              'buy': 14222,
              'sell': 14522
          },
          refresh_date: '2021-05-24T00:00:00.000Z',
          symbol: 'USD'
        })
        .end((err, res) => {
          res.should.have.status(400)
          done()
        })
      })
    }),
  // describe('POST kurs duplicate', function() {
  //   it('it should return status 409', function(done) {
  //     chai.request(app)
  //       .post('/api/kurs')
  //       .send({
  //         ERate: {
  //             'buy': 'asd',
  //             'sell': 14357
  //         },
  //         TT: {
  //             'buy': 14197,
  //             'sell': 14497
  //         },
  //         BN: {
  //             'buy': 14222,
  //             'sell': 14522
  //         },
  //         refresh_date: '2021-05-24T05:36:00.000+00:00',
  //         symbol: 'USD'
  //       })
  //       .end((err, res) => {
  //         res.should.have.status(409)
  //         done()
  //       })
  //     })
  //   }),
  describe('PUT kurs success', function() {
    it('it should return status 200', function(done) {
      chai.request(app)
        .put('/api/kurs')
        .send({
          ERate: {
              'buy': 14357,
              'sell': 14357
          },
          TT: {
              'buy': 14197,
              'sell': 14497
          },
          BN: {
              'buy': 14222,
              'sell': 14522
          },
          refresh_date: '2021-05-24T05:36:00.000+00:00',
          symbol: 'USD'
        })
        .end((err, res) => {
          res.should.have.status(200)
          done()
        })
      })
    })
  })
})
