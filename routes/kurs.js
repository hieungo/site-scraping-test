var scrapingController = require('../controllers/ScrapingController')
var kursController = require('../controllers/KursController')
var express = require('express')
var router = express.Router()
var cron = require('node-cron')
const ScrapingService = require('../services/ScrapingService')

router
  .get('/indexing', scrapingController.scrape)
  .get('/kurs', kursController.validate('searchByDate'), kursController.getKurs)
  .get('/kurs/:symbol', kursController.validate('searchByDate'), kursController.getKursBySymbol)
  .delete('/kurs/:date', kursController.validate('removeByDate'), kursController.removeKursByDate)
  .post('/kurs', kursController.validate('kurs'), kursController.addKurs)
  .put('/kurs', kursController.validate('kurs'), kursController.updateKurs)

cron.schedule('0 0 * * *', async() => {
  ScrapingService.scrape()
});

module.exports = router