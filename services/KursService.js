const Kurs = require('../schemas/KursSchema')
const { v4: uuidv4 } = require('uuid')

class KursService {

    async getKurs(startDate, endDate) {
        console.log('-- Start get kurs --')
        try {
            let result = await Kurs.find({
                'refresh_date': {
                    $gte: startDate,
                    $lte: endDate
                }
            })
            console.log('-- End get kurs success --')
            return result
        } catch (err) {
            console.log(`-- End get kurs msg: ${err} --`)
        }
    }

    async getKursBySymbol(symbol, startDate, endDate) {
        console.log('-- Start get kurs by symbol --')
        try {
            let result = await Kurs.find({
                'symbol': symbol,
                'refresh_date': {
                    $gte: startDate,
                    $lte: endDate
                }
            })
            console.log('-- End get kurs by symbol success --')
            return result
        } catch (err) {
            console.log(`-- End get kurs by symbol msg: ${err} --`)
        }
    }

    async removeKursByDate(date) {
        console.log('-- Start remove kurs --')
        try {
            await Kurs.remove({ 'refresh_date':  date })
            console.log('-- End remove kurs success --')
        } catch (err) {
            console.log(`-- End remove kurs msg ${err} --`)
        }
    }

    async addKurs(req) {
        console.log('-- Start add kurs --')
        let kurs = new Kurs(req)
        kurs._id = uuidv4()
        try {
            let currentRecord = await Kurs.findOne({
                'symbol': kurs.symbol,
                'refresh_date': kurs.refresh_date
            })
            if (!currentRecord) kurs.save()
            console.log('-- Start add kurs success --')
        } catch (err) {
            console.log(`-- End add kurs msg ${err} --`)
        }
    }

    async updateKurs(req) {
        console.log('-- Start update kurs --')
        let kurs = new Kurs(req)
        try {
            let currentRecord = await Kurs.findOne({
                'symbol': kurs.symbol,
                'refresh_date': kurs.refresh_date
            })

            if (currentRecord) {
                Kurs.update({_id: currentRecord._id}, kurs, {upsert:true}, (err, kurs) => {
                    if (err) {
                        console.log(`-- End update kurs msg ${err} --`)
                    }
                })
            }
            console.log('-- End update kurs success --')
        } catch (err) {
            console.log(`-- End update kurs msg ${err} --`)
        }
    }

    async isKursExist(symbol, refreshDate) {
        let currentRecord = await Kurs.findOne({
            'symbol': symbol,
            'refresh_date': refreshDate
        })
        console.log(currentRecord)
        if (currentRecord) return true
        return false
    }
}

module.exports = new KursService()