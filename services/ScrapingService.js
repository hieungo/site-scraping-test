const got = require('got')
const jsdom = require('jsdom')
const {JSDOM} = jsdom
const Kurs = require('../schemas/KursSchema')
const CONSTANT = require('../utils/constant')
const { v4: uuidv4 } = require('uuid')

class ScrapingService {

    async scrape() {
        console.log('-- Start scraping --')
        return got(CONSTANT.BCA_URL).then(async(response) => {
            const dom = new JSDOM(response.body)
            let tbl = dom.window.document.querySelectorAll(CONSTANT.TABLE_ROW)
            let refreshDate = new Date(dom.window.document.querySelector(CONSTANT.REFRESH_DATE).textContent)
            for (let i = 0; i < tbl.length; i++) {
                let tmp = new Kurs()
                tmp.refresh_date = refreshDate
                tmp._id = uuidv4()
                tmp.symbol = tbl[i].getAttribute(CONSTANT.CODE)
                tbl[i].querySelectorAll(CONSTANT.RATE_TYPE).forEach(t => {
                    let rate_type = t.getAttribute(CONSTANT.RATE_TYPE_ATTR).split(CONSTANT.RATE_TYPE_DELIMETER)
                    if (rate_type[1] == CONSTANT.BUY) {
                        tmp[rate_type[0]].buy = parseFloat(t.textContent.replace(/,/g, ''))
                    } else
                        tmp[rate_type[0]].sell = parseFloat(t.textContent.replace(/,/g, ''))
                })
                try {
                    let currentRecord = await Kurs.findOne({
                        'symbol': tmp.symbol,
                        'refresh_date': tmp.refresh_date
                    })
                    if (!currentRecord) tmp.save()
                } catch (err) {
                    console.log(`-- End scraping error: ${err} --`)
                }
            }
            console.log('-- End scraping success --')
          }).catch(err => {
            console.log(`-- End scraping error: ${err} --`)
            throw err
          });
    }

}

module.exports = new ScrapingService()